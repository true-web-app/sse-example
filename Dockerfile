FROM python:3.11-slim AS builder
RUN apt-get update \
    && apt-get install build-essential -y \
    && apt-get clean
WORKDIR /src

COPY requirements.txt .
RUN pip install --user -r ./requirements.txt

FROM python:3.11-slim AS simple

COPY --from=builder /root/.local /root/.local

ENV PATH=/root/.local/bin:$PATH

EXPOSE 80
COPY simple.py .
CMD python simple.py
